class User < ActiveRecord::Base
	has_many :events, through: :userevent
	has_many :groups, through: :usergroup
	has_many :interests, through: :userinterest

	#OLAWORK
	#---------------------------------------
  attr_accessor :password 
  before_save :encrypt_password

  validates :username, presence: true , length:{ minimum: 2 }
  # using valid_email
  validates :email, presence: true, uniqueness: true
  validates :password, confirmation: true ,length: { in: 6..20 }
  validates :password_confirmation, presence: true 


  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

def self.authenticate(email, password)
  user = find_by_email(email)
  if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
    user
  else
    nil
  end
end


	#--------------------------------------------
  #ForImage
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

end

class Group < ActiveRecord::Base
  belongs_to :user
  has_many :users, through: :usergroup
  has_many :interests, through: :groupinterest
  has_many :events
  

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

#ForDrop Down Menu in Citys
CITYS = ["Alex", "Cairo", "SharmElsheik", "Menofia"]

#FOR search by citys
def self.search(search)
  where("city LIKE ?", "%#{search}%")
  # where("content LIKE ?", "%#{search}%")
end

#TO search by distance
geocoded_by :name, :latitude => :lat, :longitude => :lng
end

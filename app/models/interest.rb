class Interest < ActiveRecord::Base
	has_many :users, through: :userinterest
end

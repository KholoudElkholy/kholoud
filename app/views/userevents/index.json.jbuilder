json.array!(@userevents) do |userevent|
  json.extract! userevent, :id, :user_id, :event_id
  json.url userevent_url(userevent, format: :json)
end

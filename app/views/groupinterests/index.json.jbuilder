json.array!(@groupinterests) do |groupinterest|
  json.extract! groupinterest, :id, :group_id, :interest_id
  json.url groupinterest_url(groupinterest, format: :json)
end

json.array!(@users) do |user|
  json.extract! user, :id, :username, :email, :password, :img, :lng, :lat, :isadmin
  json.url user_url(user, format: :json)
end

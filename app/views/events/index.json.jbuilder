json.array!(@events) do |event|
  json.extract! event, :id, :name, :desc, :group_id, :meetDate, :lng, :lat, :img
  json.url event_url(event, format: :json)
end

json.array!(@userinterests) do |userinterest|
  json.extract! userinterest, :id, :user_id, :interest_id
  json.url userinterest_url(userinterest, format: :json)
end

class GroupinterestsController < ApplicationController
  before_action :set_groupinterest, only: [:show, :edit, :update, :destroy]

  # GET /groupinterests
  # GET /groupinterests.json
  def index
    @groupinterests = Groupinterest.all
  end

  # GET /groupinterests/1
  # GET /groupinterests/1.json
  def show
  end

  # GET /groupinterests/new
  def new
    @groupinterest = Groupinterest.new
  end

  # GET /groupinterests/1/edit
  def edit
  end

  # POST /groupinterests
  # POST /groupinterests.json
  def create
    @groupinterest = Groupinterest.new(groupinterest_params)

    respond_to do |format|
      if @groupinterest.save
        format.html { redirect_to @groupinterest, notice: 'Groupinterest was successfully created.' }
        format.json { render :show, status: :created, location: @groupinterest }
      else
        format.html { render :new }
        format.json { render json: @groupinterest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groupinterests/1
  # PATCH/PUT /groupinterests/1.json
  def update
    respond_to do |format|
      if @groupinterest.update(groupinterest_params)
        format.html { redirect_to @groupinterest, notice: 'Groupinterest was successfully updated.' }
        format.json { render :show, status: :ok, location: @groupinterest }
      else
        format.html { render :edit }
        format.json { render json: @groupinterest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groupinterests/1
  # DELETE /groupinterests/1.json
  def destroy
    @groupinterest.destroy
    respond_to do |format|
      format.html { redirect_to groupinterests_url, notice: 'Groupinterest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_groupinterest
      @groupinterest = Groupinterest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def groupinterest_params
      params.require(:groupinterest).permit(:group_id, :interest_id)
    end
end

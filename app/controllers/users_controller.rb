class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
 ##OLA
  #http_basic_authenticate_with name: "ola", password: "123456789", except: [:index, :show]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    ###OLA
    @user = User.find(params[:id])
     #-------------------------------------------------------
        #ForEvents
    @alluserevents = Userevent.where("user_id = ?",@user.id)
    @allusergroups = Usergroup.where("user_id = ?",@user.id)
     #-------------------------------------------------------
        
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        #-------------------------------------------------------
        #ForSending Mail
        UserNotifier.send_signup_email(@user).deliver
        # redirect_to(@user, :notice => 'User created')
        #-------------------------------------------------------
        #OLA SESSION
        # session[:user_id] = user.id
        #--------------------------------------

        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        # render :action => 'new'
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
   
  end
end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      #OLA WORK
      params.require(:user).permit(:username, :email ,:password ,:password_confirmation ,:password_salt ,:password_hash,:img, :lng, :lat, :isadmin, :avatar) 
      #-------------------------------------------
      #params.require(:user).permit(:username, :email, :password, :img, :lng, :lat, :isadmin)
    end
end

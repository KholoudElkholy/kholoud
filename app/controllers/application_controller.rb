class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
#OLA WORK
helper_method :current_user 

private

def current_user
  @current_user ||= User.find(session[:user_id]) if session[:user_id]
end

before_filter :require_login , except: [ :create, :new ]



private 
 def require_login 
	unless logged_in? 
		flash[:error] = "You must be logged in to access this section" 
		redirect_to url_for(:controller => 'sessions',:action => 'new') # halts request cycle 
	end 
 end


end



# The logged_in? method simply returns true if the user is logged # in and false otherwise. It does this by "booleanizing" the # current_user method we created previously using a double ! operator. # Note that this is not common in Ruby and is discouraged unless you # really mean to convert something into true or false. 

def logged_in? 
!!current_user 
end
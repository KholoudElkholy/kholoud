class UsereventsController < ApplicationController
  before_action :set_userevent, only: [:show, :edit, :update, :destroy]

  # GET /userevents
  # GET /userevents.json
  def index
    @userevent = Userevent.all
  end

  # GET /userevents/1
  # GET /userevents/1.json
  def show
  end

  # GET /userevents/new
  def new
    @userevent = Userevent.new
  end

  # GET /userevents/1/edit
  def edit
  end

  # POST /userevents
  # POST /userevents.json
  def create
    @userevent = Userevent.new(userevent_params)

    respond_to do |format|
      if @userevent.save
        format.html { redirect_to @userevent, notice: 'Userevent was successfully created.' }
        format.json { render :show, status: :created, location: @userevent }
      else
        format.html { render :new }
        format.json { render json: @userevent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /userevents/1
  # PATCH/PUT /userevents/1.json
  def update
    respond_to do |format|
      if @userevent.update(userevent_params)
        format.html { redirect_to @userevent, notice: 'Userevent was successfully updated.' }
        format.json { render :show, status: :ok, location: @userevent }
      else
        format.html { render :edit }
        format.json { render json: @userevent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /userevents/1
  # DELETE /userevents/1.json
  def destroy
    @userevent.destroy
    respond_to do |format|
      format.html { redirect_to userevents_url, notice: 'Userevent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_userevent
      @userevent = Userevent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def userevent_params
      params.require(:userevent).permit(:user_id, :event_id)
    end
end

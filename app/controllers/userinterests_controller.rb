class UserinterestsController < ApplicationController
  before_action :set_userinterest, only: [:show, :edit, :update, :destroy]

  # GET /userinterests
  # GET /userinterests.json
  def index
    @userinterests = Userinterest.all
  end

  # GET /userinterests/1
  # GET /userinterests/1.json
  def show
  end

  # GET /userinterests/new
  def new
    @userinterest = Userinterest.new
  end

  # GET /userinterests/1/edit
  def edit
  end

  # POST /userinterests
  # POST /userinterests.json
  def create
    @userinterest = Userinterest.new(userinterest_params)

    respond_to do |format|
      if @userinterest.save
        format.html { redirect_to @userinterest, notice: 'Userinterest was successfully created.' }
        format.json { render :show, status: :created, location: @userinterest }
      else
        format.html { render :new }
        format.json { render json: @userinterest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /userinterests/1
  # PATCH/PUT /userinterests/1.json
  def update
    respond_to do |format|
      if @userinterest.update(userinterest_params)
        format.html { redirect_to @userinterest, notice: 'Userinterest was successfully updated.' }
        format.json { render :show, status: :ok, location: @userinterest }
      else
        format.html { render :edit }
        format.json { render json: @userinterest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /userinterests/1
  # DELETE /userinterests/1.json
  def destroy
    @userinterest.destroy
    respond_to do |format|
      format.html { redirect_to userinterests_url, notice: 'Userinterest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_userinterest
      @userinterest = Userinterest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def userinterest_params
      params.require(:userinterest).permit(:user_id, :interest_id)
    end
end

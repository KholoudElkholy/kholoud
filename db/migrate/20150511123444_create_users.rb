class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :email
      t.string :password
      #ola
      t.string :password_hash
      t.string :password_salt
      #----------------
      t.string :img
      t.decimal :lng
      t.decimal :lat
      t.boolean :isadmin, default: 0
      t.timestamps null: false

    end
  end
end

class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.references :user, index: true, foreign_key: true
      t.text :desc
      t.string :memberName
      t.string :city
      t.decimal :lng
      t.decimal :lat

      t.timestamps null: false
    end
  end
end

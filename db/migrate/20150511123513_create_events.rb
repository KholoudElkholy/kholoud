class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :desc
      t.references :group, index: true, foreign_key: true
      t.datetime :meetDate
      t.decimal :lng
      t.decimal :lat
      t.string :img

      t.timestamps null: false
    end
  end
end

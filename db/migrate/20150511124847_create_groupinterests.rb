class CreateGroupinterests < ActiveRecord::Migration
  def change
    create_table :groupinterests do |t|
      t.references :group, index: true, foreign_key: true
      t.references :interest, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

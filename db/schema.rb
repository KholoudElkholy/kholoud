# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150512160757) do

  create_table "comments", force: :cascade do |t|
    t.text     "desc",       limit: 65535
    t.integer  "event_id",   limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "comments", ["event_id"], name: "index_comments_on_event_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.text     "desc",                limit: 65535
    t.integer  "group_id",            limit: 4
    t.datetime "meetDate"
    t.decimal  "lng",                               precision: 10
    t.decimal  "lat",                               precision: 10
    t.string   "img",                 limit: 255
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
  end

  add_index "events", ["group_id"], name: "index_events_on_group_id", using: :btree

  create_table "groupinterests", force: :cascade do |t|
    t.integer  "group_id",    limit: 4
    t.integer  "interest_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "groupinterests", ["group_id"], name: "index_groupinterests_on_group_id", using: :btree
  add_index "groupinterests", ["interest_id"], name: "index_groupinterests_on_interest_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.integer  "user_id",             limit: 4
    t.text     "desc",                limit: 65535
    t.string   "memberName",          limit: 255
    t.string   "city",                limit: 255
    t.decimal  "lng",                               precision: 10
    t.decimal  "lat",                               precision: 10
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
  end

  add_index "groups", ["user_id"], name: "index_groups_on_user_id", using: :btree

  create_table "interests", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.text     "desc",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "userevents", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "event_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "userevents", ["event_id"], name: "index_userevents_on_event_id", using: :btree
  add_index "userevents", ["user_id"], name: "index_userevents_on_user_id", using: :btree

  create_table "usergroups", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "group_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "usergroups", ["group_id"], name: "index_usergroups_on_group_id", using: :btree
  add_index "usergroups", ["user_id"], name: "index_usergroups_on_user_id", using: :btree

  create_table "userinterests", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.integer  "interest_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "userinterests", ["interest_id"], name: "index_userinterests_on_interest_id", using: :btree
  add_index "userinterests", ["user_id"], name: "index_userinterests_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username",            limit: 255
    t.string   "email",               limit: 255
    t.string   "password",            limit: 255
    t.string   "password_hash",       limit: 255
    t.string   "password_salt",       limit: 255
    t.string   "img",                 limit: 255
    t.decimal  "lng",                             precision: 10
    t.decimal  "lat",                             precision: 10
    t.boolean  "isadmin",             limit: 1,                  default: false
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
  end

  add_foreign_key "comments", "events"
  add_foreign_key "comments", "users"
  add_foreign_key "events", "groups"
  add_foreign_key "groupinterests", "groups"
  add_foreign_key "groupinterests", "interests"
  add_foreign_key "groups", "users"
  add_foreign_key "userevents", "events"
  add_foreign_key "userevents", "users"
  add_foreign_key "usergroups", "groups"
  add_foreign_key "usergroups", "users"
  add_foreign_key "userinterests", "interests"
  add_foreign_key "userinterests", "users"
end

require 'test_helper'

class GroupinterestsControllerTest < ActionController::TestCase
  setup do
    @groupinterest = groupinterests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:groupinterests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create groupinterest" do
    assert_difference('Groupinterest.count') do
      post :create, groupinterest: { group_id: @groupinterest.group_id, interest_id: @groupinterest.interest_id }
    end

    assert_redirected_to groupinterest_path(assigns(:groupinterest))
  end

  test "should show groupinterest" do
    get :show, id: @groupinterest
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @groupinterest
    assert_response :success
  end

  test "should update groupinterest" do
    patch :update, id: @groupinterest, groupinterest: { group_id: @groupinterest.group_id, interest_id: @groupinterest.interest_id }
    assert_redirected_to groupinterest_path(assigns(:groupinterest))
  end

  test "should destroy groupinterest" do
    assert_difference('Groupinterest.count', -1) do
      delete :destroy, id: @groupinterest
    end

    assert_redirected_to groupinterests_path
  end
end

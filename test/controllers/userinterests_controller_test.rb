require 'test_helper'

class UserinterestsControllerTest < ActionController::TestCase
  setup do
    @userinterest = userinterests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:userinterests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create userinterest" do
    assert_difference('Userinterest.count') do
      post :create, userinterest: { interest_id: @userinterest.interest_id, user_id: @userinterest.user_id }
    end

    assert_redirected_to userinterest_path(assigns(:userinterest))
  end

  test "should show userinterest" do
    get :show, id: @userinterest
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @userinterest
    assert_response :success
  end

  test "should update userinterest" do
    patch :update, id: @userinterest, userinterest: { interest_id: @userinterest.interest_id, user_id: @userinterest.user_id }
    assert_redirected_to userinterest_path(assigns(:userinterest))
  end

  test "should destroy userinterest" do
    assert_difference('Userinterest.count', -1) do
      delete :destroy, id: @userinterest
    end

    assert_redirected_to userinterests_path
  end
end
